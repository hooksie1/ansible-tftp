import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_tftp_installed(host):
    tftp = host.package("tftp-server")
    assert tftp.is_installed


def test_tftp_running(host):
    tftp_service = host.service("tftp")
    assert tftp_service.is_running
    assert tftp_service.is_enabled


def test_tftp_dir(host):
    file = host.file("/var/lib/tftpboot/pxelinux.cfg")
    assert file.is_directory


def test_7_dir(host):
    file = host.file("/var/lib/tftpboot/7")
    assert file.is_directory


def test_vesa_file(host):
    file = host.file("/var/lib/tftpboot/vesamenu.c32")
    assert file.exists
